# Free Epic Games Checker

## Setup

- first download the geckodriver from [HERE](https://github.com/mozilla/geckodriver/releases)

- secondly, do `pip install -r requirements.txt --user`

## Running

- run `python3 run.py`
- enjoy