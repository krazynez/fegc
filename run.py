#!/usr/bin/env python3
'''
Title: FEGC (Free Epic Game Checker)
Author: Brennen Murphy
Date: December 26, 2019
Version: 1.0
'''
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.expected_conditions import (
    presence_of_element_located)
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, TimeoutException

def main():
    try:
        check = input('Is it the holiday season? y/n: ')
        string='''\nWelcome, pulling names of free games now from Epic
--------------------------------------------------'''
        print(string)
        _URL = "https://www.epicgames.com/store/en-US/collection/free-games-collection"
        options = Options()
        options.headless = True
        driver = webdriver.Firefox(options=options)
        driver.get(_URL)
        # Check if it is the holiday season or not
        if check.lower() == 'n':
            get_that_shit = presence_of_element_located((By.TAG_NAME, 'h3'))
        else:
            get_that_shit = presence_of_element_located((By.CLASS_NAME, 'AvailabilityStatusBar-free_7128c337'))
        wait = WebDriverWait(driver, 5)
        wait.until(get_that_shit)
        if check.lower() == 'n':
            tags = driver.find_elements_by_class_name('h3')
        else:
            tags = driver.find_elements_by_class_name('Card-content_070be76b')
        for tag in tags:
            # Checks to see which tag has 'free now', its the only one available currently
            if "free now" in tag.text.lower().strip():
                # Removes Free Now from top, unessasary, but left the experation date
                print(tag.text[8:])
            else:
                pass
        print('--------------------------------------------------')
        driver.quit()
    except FileNotFoundError:
        print('You need the geckodriver installed in your PATH link: [ https://github.com/mozilla/geckodriver/releases ]')
    except TimeoutException:
        print("Are you sure it is not a holiday season sale? Rerun and say 'y' instead")

if __name__ == '__main__':
    main()
